# Virtual environments

## Prerequisites

### [VirtualBox] (http://vagrantup.com)
> Todo: Describe what VirtualBox is and how it works.

### [Vagrant] (http://ansible.cc)
> Todo: Describe what Vagrant is and how it works.

### [Ansible] (http://virtualbox.com)
> Todo: Describe what Ansible is and how it works.

### [Vagrant Ansible] (https://github.com/dsander/vagrant-ansible)
> Todo: Describe what Vagrant Ansible is and how it works.

## Installation

### Vagrant & VirtualBox
Installing both, Vagrant and VirtualBox is rather simple.

``` sudo apt-get install vagrant ```

This will install Vagrant and all its dependencies including
VirtualBox.

### Ansible
Ansible requires a few additional (python) packages before it can be
installed ([yaml] (http://www.yaml.org/), [jinja2] (http://jinja.pocoo.org/) and [paramiko] (https://github.com/paramiko/paramiko/)).

``` sudo apt-get install python-jinja2 python-yaml python-paramiko python-software-properties ```

Once the aforementioned dependencies have been successfully
installed you can continue with installing Ansible.

Ansible is not yet packaged for most distributions. Therefore
you probably have to install it through ``` make install ```.

```
git clone git://github.com/ansible/ansible.git
cd ansible
sudo make install
```

### Vagrant Ansible
> Todo: Describe how vagrant ansible and ansible can be installed.

## References
* [Vagrant documentation] (http://docs.vagrantup.com/v1/docs/index.html)
* [Ansible documentation] (http://ansible.cc/docs)
* [Ansible repository] (https://github.com/ansible/ansible)
* [Ansible Vagrant repository] (https://github.com/dsander/vagrant-ansible)